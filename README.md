# Countdown timer
## (main) input, start, pause countdown timer with OOP.
1. Upon click of start countdown start, whereas on pause, it pauses.
2. Parse input string value to float and do the subtract. Use getters and setters.
3. Make a conditional that stipulates input value higher than 0;
4. Takes optional callback parameters such as onStart, onTick, onComplete. Since it is optional, a condition should apply to check it it there, if so then those parameters/functions should be invoked.
5. Extracting timer code logic to different file.
6. Draw a svg circle.
7. Make use of fill, stroke, stroke-width, stroke-dasharray, stroke-dashoffset properties of circle element.
8. Get attribute radius and calculate perimeter and with the help of it subtract dashoffset.
    

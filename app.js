
const durationInput = document.querySelector('.duration');
const startBtn = document.querySelector('.start');
const pauseBtn = document.querySelector('.pause');
const resetBtn = document.querySelector(".reset");

const circle = document.querySelector('circle');


console.dir(durationInput)

resetBtn.addEventListener("click", () => {
    durationInput.value = "8"
})


const perimeter = 2 * circle.getAttribute("r") * Math.PI;
circle.setAttribute('stroke-dasharray', perimeter);

let duration;


const timer = new Timer(durationInput, startBtn, pauseBtn, {
    onStart(totalDuration) {
        duration = totalDuration;
    },
    onTick(timeRemain) {
        circle.setAttribute('stroke-dashoffset', perimeter * timeRemain / duration - perimeter);

    },
    onComplete() {
        console.log("Time has stopped")

    }
});
